import Warehouse from "./containers/Warehouse/Warehouse";

function App() {
  return (
    <div className="App">
      <Warehouse />
    </div>
  );
}

export default App;
