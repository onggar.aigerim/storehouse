import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import FormNewProduct from "../../components/FormNewProduct/FormNewProduct";
import ProductList from "../../components/ProductList/ProductList";

const Warehouse = () => {
  const randomNum = () => {
    return Math.random().toString(36).substr(2, 3);
  };

  const [product, setProduct] = useState({
    title: "",
    price: "",
    stock: ""
  });

  const [products, setProducts] = useState([
    {name: "Pro Balance Biotics Toner", price: "4800", stock: 8, id: uuidv4(), disabledInfo: false},
    {name: "Don't Touch My Azacid", price: "12000", stock: 6, id: uuidv4(), disabledInfo: false},
    {name: "Vital Hydra Solution", price: "4500", stock: 10, id: uuidv4(), disabledInfo: false},
    {name: "GRAND Amino Cushion Cream", price: "25000", stock: 8, id: uuidv4(), disabledInfo: false},
  ]);

  const onClickMinus = (id) => {
    const index = products.findIndex(p => p.id === id);
    const copyProducts = [...products];
    copyProducts[index].stock = copyProducts[index].stock - 1;
    if (copyProducts[index].stock <= 0) {
      copyProducts[index].disabledInfo = true;
    }
    setProducts(copyProducts);
  };

  const changeProductVal = (e) => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value
    });
  };

  const createNewProduct = (e) => {
    e.preventDefault();

    let price;
    let stock;

    const copyProducts = [...products];
    const findProduct = copyProducts.map(p => p.name.includes(product.title));
    const index = findProduct.findIndex(p => p === true);

    if (product.stock === "" || parseInt(product.stock) < 0) {
      stock = 0;
    } else {
      stock = parseInt(product.stock)
    };

    if (product.price === "" || parseInt(product.price) < 0) {
      price = 0;
    } else {
      price = product.price;
    };
  
    if (product.title !== "" ) {
      if (index >= 0) {
        const question = window.confirm("Хотите заменить текущую позицию?");
        if (question) {
          if (product.price === "") {
            price = copyProducts[index].price
          } else {
            price = product.price;
          };

          if (product.stock === "") {
            stock = copyProducts[index].stock;
          } else {
            stock = copyProducts[index].stock + parseInt(product.stock);
          };
          copyProducts[index] = {
            ...copyProducts[index],
            price: price,
            stock: stock,
            id: uuidv4(), 
            disabledInfo: false
          };
        } else {
          const newProduct = {
            name: product.title + ` ${randomNum()}`,
            price: price, 
            stock: stock, 
            id: uuidv4(), 
            disabledInfo: false
          };
          copyProducts.push(newProduct);
        }
      } else {
        const newProduct = {
          name: product.title,
          price: price, 
          stock: stock, 
          id: uuidv4(), 
          disabledInfo: false
        };
        copyProducts.push(newProduct);
      };
    };
    setProducts(copyProducts);
    setProduct({
      title: "",
      price: "",
      stock: ""
    });
  };

  const sortProductTitleUp = () => {
    const copyProducts = [...products];
    copyProducts.sort((a, b) => {
      if (a.name > b.name) {
        return 1;
      };
      if (a.name < b.name) {
        return -1;
      };
      return 0;
    });

    setProducts(copyProducts);
  };

  const sortProductTitleDown = () => {
    const copyProducts = [...products];
    copyProducts.sort((a, b) => {
      if (a.name > b.name) {
        return -1;
      };
      if (a.name < b.name) {
        return 1;
      };
      return 0;
    });

    setProducts(copyProducts);
  };

  const sortProductPriceUp = () => {
    const copyProducts = [...products];
    copyProducts.sort((a, b) => {
      return b.price - a.price;
    });

    setProducts(copyProducts);
  };

  const sortProductPriceDown = () => {
    const copyProducts = [...products];
    copyProducts.sort((a, b) => {
      return a.price - b.price;
    });
    setProducts(copyProducts);
  };

  const sortProductStockUp = () => {
    const copyProducts = [...products];
    copyProducts.sort((a, b) => {
      return b.stock - a.stock;
    });
    setProducts(copyProducts);
  };

  const sortProductStockDown = () => {
    const copyProducts = [...products];
    copyProducts.sort((a, b) => {
      return a.stock - b.stock;
    });
    setProducts(copyProducts);
  };

  return <div>
    <ProductList 
      products={products} 
      minusHandler={onClickMinus}
      sortTitleUp={sortProductTitleUp}
      sortTitleDown={sortProductTitleDown}
      sortPriceUp={sortProductPriceUp}
      sortPriceDown={sortProductPriceDown}
      sortStockUp={sortProductStockUp}
      sortStockDown={sortProductStockDown}
    />
    <FormNewProduct 
      changeHandler={(e) => changeProductVal(e)}
      submitHandler={(e) => createNewProduct(e)}
      titleVal={product.title}
      priceVal={product.price} 
      stockVal={product.stock}
    />
  </div>
};

export default Warehouse;