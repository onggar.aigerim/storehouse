import styled from "styled-components";

const TdStock = styled.td`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Button = styled.button`
  background: #f4433669;
  border: 1px solid;
  font-weight: 700;
  cursor: pointer;
`;

const Product = ({
  num, 
  title, 
  price, 
  stock, 
  minusHandler, 
  disabled
}) => {
  return <tr>
    <td>{num}</td>
    <td>{title}</td>
    <td>{price}</td>
    <TdStock>{stock}
      <Button 
        onClick={minusHandler}
        disabled={disabled}
      >
        -
      </Button>
    </TdStock>
  </tr>
}

export default Product;