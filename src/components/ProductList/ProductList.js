import styled from "styled-components";
import ProductsTable from "./ProductsTable/ProductsTable";

const Div = styled.div`
  border: 1px solid;
  border-radius: 8px 8px 0 0;
  background: #146a93;
  margin: 20px;
  & p {
    text-align: center;
    text-transform: uppercase;
    font-size: 16px;
    line-height: 24px;
    padding: 16px 24px;
    color: #fff;
    border-radius: 8px 8px 0 0;
  };
`;

const ProductList = ({
  products, 
  minusHandler, 
  sortTitleUp,
  sortTitleDown,
  sortPriceUp,
  sortPriceDown,
  sortStockUp,
  sortStockDown
}) => {
  let productsList;
  const productsStock = [];

  products.forEach(product => {
    if (product.stock > 0) {
      productsStock.push(product);
    };
  });
  
  if (products.length > 0 && productsStock.length > 0) {
    productsList = <ProductsTable 
      products={products}
      minusHandler={minusHandler}
      sortTitleUp={sortTitleUp}
      sortTitleDown={sortTitleDown}
      sortPriceUp={sortPriceUp}
      sortPriceDown={sortPriceDown}
      sortStockUp={sortStockUp}
      sortStockDown={sortStockDown}
    />;
  } else {
    productsList = <Div>
      <p>No products avaiable</p>
    </Div>;
  };

  return productsList;
};

export default ProductList;