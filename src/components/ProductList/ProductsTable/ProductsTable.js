import styled from "styled-components";
import Product from "../Product/Product";

const Div = styled.div`
  border: 1px solid;
  border-radius: 8px 8px 0 0;
  background: #effaff;
  margin: 20px;
  & div {
    background: #146a93;
    text-transform: uppercase;
    font-size: 16px;
    line-height: 24px;
    padding: 16px 24px;
    color: #fff;
    border-radius: 8px 8px 0 0;
  }
`;

const Table = styled.table`
  padding: 20px;
  padding-top: 0;
  width: 100%;
  table-layout: fixed;
  
  & td {
    border-bottom: 1px solid khaki;
    line-height: 40px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  };

  & th {
    text-align: start;
    border-bottom: 2px solid khaki;
    line-height: 50px;
    & span {
      display: inline-block;
      vertical-align: middle;
      & p {
        margin: 15px;
        line-height: 0;
        cursor: pointer;
      }
    }
  };

  & .num {
    width: 5%;
  };

  & .title {
    width: 50%;
  };

  & .price {
    width: 25%;
  };
  
  & .stock {
    width: 20%;
  };
`;

const ProductsTable = ({
  products, 
  minusHandler,
  sortTitleUp,
  sortTitleDown,
  sortPriceUp,
  sortPriceDown,
  sortStockUp,
  sortStockDown}) => {
  return <Div>
    <div>Products in stock</div> 
    <Table>
      <thead>
        <tr>
          <th className="num">#</th>
          <th className="title">
            Title 
            <span>
              <p onClick={sortTitleUp} > ▲ </p>
              <p onClick={sortTitleDown}> ▼ </p>
            </span> 
          </th>
          <th className="price">
            Price &#x20b8;
            <span>
              <p onClick={sortPriceUp} > ▲ </p>
              <p onClick={sortPriceDown}> ▼ </p>
            </span>
          </th>
          <th className="stock">
            Stock
            <span>
              <p onClick={sortStockUp} > ▲ </p>
              <p onClick={sortStockDown}> ▼ </p>
            </span>
          </th>
        </tr>
      </thead>
      <tbody>
        {products.map((product, i) => {
          return <Product
            num={i + 1 + "."}
            key={product.id}
            title={product.name}
            price={product.price}
            stock={product.stock}
            disabled={product.disabledInfo}
            minusHandler={() => minusHandler(product.id)}
          />
        })}
      </tbody>
    </Table>
  </Div>
};

export default ProductsTable;