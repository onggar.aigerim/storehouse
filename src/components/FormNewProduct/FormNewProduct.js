import styled from "styled-components";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin: 20px;
  align-items: center;
  & h2 {
    text-align: center;
    border-bottom: 1px solid #f59200;
    width: 50%;
    padding: 20px 0;
    margin: 0 0 10px 0;
  }
  
  & input {
    width: 80%;
    margin: 10px 0;
    padding: 10px;
    font-size: 18px;
  }

  & button {
    font-size: 18px;
    cursor: pointer;
    background: antiquewhite;
    border: 1px solid;
    border-radius: 5px;
    margin: 10px;
    padding: 5px 10px;
  }
`;

const FormNewProduct = ({
  changeHandler, 
  titleVal, 
  priceVal, 
  stockVal,
  submitHandler
}) => {
  return <Form onSubmit={submitHandler}>
    <h2>Create New Product</h2>
    <input 
      type="text"
      name="title"
      placeholder="Title"
      value={titleVal}
      onChange={changeHandler}
    />
    <input 
      type="number"
      name="price"
      placeholder="Price"
      value={priceVal}
      onChange={changeHandler}
    />
    <input 
      type="number"
      name="stock"
      placeholder="Stock"
      value={stockVal}
      onChange={changeHandler}
    />
    <button>Create Product</button>
  </Form>
}

export default FormNewProduct;